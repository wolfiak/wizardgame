import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by pacio on 22.08.2017.
 */
public class BufferdImageLoader {


    private BufferedImage image;

    public BufferedImage loadImage(String path){
        try {
            image= ImageIO.read(getClass().getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}
