import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by pacio on 24.08.2017.
 */
public class MouseInput extends MouseAdapter {

    private Handler handler;
    private Camera camera;
    private Game game;
    private  SpriteSheet ss;
    public MouseInput(Handler handler,Camera camera, Game game,SpriteSheet ss){

        this.handler=handler;
        this.camera=camera;
        this.game=game;
        this.ss=ss;
    }

    public void mousePressed(MouseEvent e){
        int mx=(int) (e.getX() + camera.getX());
        int my=(int) (e.getY() + camera.getY());


        for(int n=0;n<handler.getOb().size();n++){
            GameObject g=handler.getOb().get(n);
            if(g.getId() == ID.PLAYER && game.getAmmo() >= 1){
                handler.addObject(new Bullet(g.getX()+16,g.getY()+24,ID.BULLET,handler,mx,my,ss));
                game.setAmmo(game.getAmmo()-1);
            }
        }

        /*for(GameObject g : handler.getOb()){
            if(g.getId() == ID.PLAYER){
                handler.addObject(new Bullet(g.getX()+16,g.getY()+24,ID.BULLET,handler,mx,my));
            }
        }
        */
    }
}
