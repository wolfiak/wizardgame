import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by pacio on 16.09.2017.
 */
public class AmmoPack extends GameObject {

    private BufferedImage create_image;

    public AmmoPack(int x, int y, ID id, SpriteSheet ss) {
        super(x, y, id,ss);

        create_image=ss.grabImage(6,2,32,32);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(create_image,x,y,null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }
}
