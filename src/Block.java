import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by pacio on 22.08.2017.
 */
public class Block extends  GameObject {

    public BufferedImage block_image;
    public Block(int x, int y, ID id, SpriteSheet ss){
        super(x,y,id,ss);
        block_image=ss.grabImage(5,2,32,32);


    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {

        g.drawImage(block_image,x,y,null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }
}
