import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by pacio on 22.08.2017.
 */
public class KeyInput extends KeyAdapter {

    private Handler handler;

    public KeyInput(Handler handler){
        this.handler=handler;
    }

    public void keyPressed(KeyEvent e){
        int key=e.getKeyCode();

        for(GameObject g: handler.getOb()){
            if(g.getId() == ID.PLAYER){
                if(key == KeyEvent.VK_W){
                    handler.setUp(true);
                }
                if(key == KeyEvent.VK_S){
                    handler.setDown(true);
                }
                if(key == KeyEvent.VK_D){
                    handler.setRight(true);
                }
                if(key == KeyEvent.VK_A){
                    handler.setLeft(true);
                }
            }
        }
    }
    public void keyReleased(KeyEvent e){
        int key=e.getKeyCode();

        for(GameObject g: handler.getOb()){
            if(g.getId() == ID.PLAYER){
                if(key == KeyEvent.VK_W){
                    handler.setUp(false);
                }else if(key == KeyEvent.VK_S){
                    handler.setDown(false);
                }else if(key == KeyEvent.VK_D){
                    handler.setRight(false);
                }else if(key == KeyEvent.VK_A){
                    handler.setLeft(false);
                }
            }
        }
    }

}
