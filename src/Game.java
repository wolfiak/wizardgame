import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

/**
 * Created by pacio on 22.08.2017.
 */
public class Game extends Canvas implements Runnable{
    private boolean isRunning=false;
    private Thread t;
    private Handler handler;
    private BufferedImage level=null;
    private BufferedImage spriteSheet=null;
    private BufferedImage floor=null;
    private Camera camera;
    private SpriteSheet ss;
    private int ammo=100;
    private  int hp=100;

    public Game(){
      new Window(1000, 563, "Giera", this);

        handler=new Handler();
        camera=new Camera(0,0);
        this.addKeyListener(new KeyInput(handler));

        BufferdImageLoader loader=new BufferdImageLoader();
        spriteSheet=loader.loadImage("/sprite_sheet.png");
        level=loader.loadImage("/wizard_level.png");
         ///handler.addObject(new Box(100,100,ID.BLOCK));
          //handler.addObject(new Box(200,100,ID.BLOCK));
        ss=new SpriteSheet(spriteSheet);
        floor=ss.grabImage(4,2,32,32);
        this.addMouseListener(new MouseInput(handler,camera,this,ss));
        loadLevel(level);

         start();
    }


    @Override
    public void run() {
        this.requestFocus();
        long lastTime=System.nanoTime();
        double amountOfTicks=60.0;
        double ns=1000000000 / amountOfTicks;
        double delta=0;
        long timer=System.currentTimeMillis();
        int frames=0;
        while(isRunning){
            long now=System.nanoTime();
            delta+=(now-lastTime) /ns;
            lastTime=now;
            while(delta >=1 ){
                tick();
                delta--;
            }
            render();
            frames++;
            if(System.currentTimeMillis() - timer > 1000){
                timer+=1000;
                frames=0;
            }
        }
        stop();
    }
    public void tick(){
        camera.tick(findPlayer());
        handler.tick();
    }
    public GameObject findPlayer(){
        for(GameObject g : handler.getOb()){
            if(g.getId() == ID.PLAYER){
                return  g;
            }
        }
        return null;
    }
    public void render(){
        BufferStrategy bs=this.getBufferStrategy();
        if(bs==null){
            this.createBufferStrategy(3);
            return;
        }

        Graphics g= bs.getDrawGraphics();
        Graphics2D g2d=(Graphics2D) g;



        g2d.translate(-camera.getX(),-camera.getY());

        for(int xx=0;xx<30*72;xx+=32){
            for(int yy=0;yy<30*72;yy+=32){
                ((Graphics2D) g).drawImage(floor,xx,yy,null);
            }
        }
        handler.render(g);
        g2d.translate(camera.getX(),camera.getY());

        g.setColor(Color.gray);
        g.fillRect(5,5,200,32);
        g.setColor(Color.green);
        g.fillRect(5,5,hp*2,32);
        g.setColor(Color.black);
        g.drawRect(5,5,200,32);

        g.setColor(Color.WHITE);
        ((Graphics2D) g).drawString("Ammo: "+ammo,5,50);

        g.dispose();
        bs.show();
    }
    private void loadLevel(BufferedImage img){
        int w=img.getWidth();
        int h=img.getHeight();

        for(int n=0;n<w;n++){
            for(int m=0;m<h;m++){
                int pixel=img.getRGB(n,m);
                int red= (pixel >> 16 )& 0xff;
                int green= (pixel >> 8) & 0xff;
                int blue=(pixel) & 0xff;

                if(red==255){
                    handler.addObject(new Block(n*32,m*32,ID.BLOCK,ss));
                }
                if(blue==255 && green==0){
                    handler.addObject(new Wizard(n*32,m*32,ID.PLAYER,handler,this,ss));
                }
                if(green == 255 && blue==0){
                    handler.addObject(new Enemy(n*32,m*32,ID.ENEMY,handler,ss));
                }
                if(green ==255 && blue==255){
                    handler.addObject(new AmmoPack(n*32,m*32,ID.CREATE,ss));
                }


            }
        }
    }

    private void start(){
        isRunning=true;
        t=new Thread(this);
        t.run();
    }
    private void stop(){
        isRunning=false;
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public static void main(String[] args){
        new Game();
    }


}
