import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by pacio on 16.09.2017.
 */
public class Enemy extends  GameObject {

    private Handler handler;
    private Random random=new Random();
    private int choose=0;
    private int hp=100;
    private BufferedImage enemyImage;

    public Enemy(int x, int y, ID id, Handler handler, SpriteSheet ss) {
        super(x, y, id,ss);
        this.handler=handler;
        enemyImage=ss.grabImage(4,1,32,32);
    }

    @Override
    public void tick() {
        x+=velx;
        y+=vely;

        choose = random.nextInt(10);


        for(int n=0;n<handler.getOb().size();n++){
            GameObject g=handler.getOb().get(n);

            if(g.getId() == ID.BLOCK){

                if(getBoundsBig().intersects(g.getBounds())){
                    x+=(velx*5) *-1;
                    y+=(vely*5) *-1;
                    velx *=-1;
                    vely *=-1;
                }else if(choose == 0 ){
                    velx = (random.nextInt(4 - -4)+ -4);
                    vely = (random.nextInt(4 - -4)+ -4);
                }
            }
            if(g.getId() == ID.BULLET){

                if(getBounds().intersects(g.getBounds())){
                    hp-=50;
                    handler.removeObject(g);

                }

            }

        }
        if(hp<=0){
            handler.removeObject(this);
        }


    }

    @Override
    public void render(Graphics g) {
       g.drawImage(enemyImage,x,y,null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }

    public Rectangle getBoundsBig(){
        return new Rectangle(x-16,y-16,64,64);
    }
}
