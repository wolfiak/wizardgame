import java.awt.*;
import java.util.LinkedList;

/**
 * Created by pacio on 22.08.2017.
 */
public class Handler {
   private  LinkedList<GameObject> ob=new LinkedList<GameObject>();
   private boolean up=false,down=false,right=false,left=false;
    public void tick(){


        for(int n=0;n< ob.size();n++){
            ob.get(n).tick();

        }
    }
    public void render(Graphics g){
       /* for(GameObject o: ob){
            o.render(g);
        }*/
        for(int n=0;n< ob.size();n++){
            GameObject o=ob.get(n);
            o.render(g);
        }
    }

    public void addObject(GameObject g){
        ob.add(g);
    }
    public void removeObject(GameObject g){
        ob.remove(g);
    }

    public LinkedList<GameObject> getOb() {
        return ob;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }
}
