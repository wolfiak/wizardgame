import java.awt.*;

/**
 * Created by pacio on 22.08.2017.
 */
public class Box extends  GameObject {


    public Box(int x, int y, ID id,SpriteSheet ss) {
        super(x, y, id,ss);

        velx=10;
    }

    @Override
    public void tick() {
        x+=velx;
        y+=vely;


    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(x,y,32,32);
    }

    @Override
    public Rectangle getBounds() {
        return null;
    }
}
