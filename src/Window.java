import javax.swing.*;
import java.awt.*;

/**
 * Created by pacio on 22.08.2017.
 */
public class Window {

    public Window(int width, int height, String title, Game g){

        JFrame frame=new JFrame(title);

        frame.setPreferredSize(new Dimension(width,height));
        frame.setMaximumSize(new Dimension(width,height));
        frame.setMinimumSize(new Dimension(width,height));

        frame.add(g);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
