import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by pacio on 22.08.2017.
 */
public class Wizard extends GameObject {

    private Handler handler;
    private Game game;
    private BufferedImage wizardImage;

    public Wizard(int x, int y, ID id, Handler handler, Game game, SpriteSheet ss){
        super(x,y,id,ss);
        this.handler=handler;
        this.game=game;
        wizardImage=ss.grabImage(1,1,32,48);
    }

    @Override
    public void tick() {
        x+=velx;
        y+=vely;

        collision();

        if(handler.isUp()){
            vely=-5;
        }else if(!handler.isDown()){
            vely=0;
        }

        if(handler.isDown()){
            vely=5;
        }else if(!handler.isUp()){
            vely=0;
        }

        if(handler.isRight()){
            velx=5;
        }else if(!handler.isLeft()){
            velx=0;
        }

        if(handler.isLeft()){
            velx=-5;
        }else if(!handler.isRight()){
            velx=0;
        }

    }
    private void collision(){
        for(int n=0;n<handler.getOb().size();n++){
            GameObject g=handler.getOb().get(n);
            if(g.getId() == ID.BLOCK){
                if(getBounds().intersects(g.getBounds())){
                    x +=velx * -1;
                    y +=vely * -1;
                }
            }

            if(g.getId() == ID.CREATE){
                if(getBounds().intersects(g.getBounds())){
                   game.setAmmo(game.getAmmo()+10);
                   handler.removeObject(g);
                }
            }

            if(g.getId() == ID.ENEMY){
                if(getBounds().intersects(g.getBounds())){
                    game.setHp(game.getHp()-1);
                }
            }
        }
    }
    @Override
    public void render(Graphics g) {
        g.drawImage(wizardImage,x,y,null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,48);
    }
}
