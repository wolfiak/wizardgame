import java.awt.*;

/**
 * Created by pacio on 24.08.2017.
 */
public class Bullet extends  GameObject {


    private Handler handler;


    public Bullet(int x, int y, ID id, Handler handler, int mx, int my, SpriteSheet ss){
        super(x,y,id,ss);
        this.handler=handler;

        velx = (mx - x) /10;
        vely = (my - y) /10;
    }

    @Override
    public void tick() {
        x += velx;
        y += vely;

        for(int n=0;n<handler.getOb().size();n++){
            GameObject g=handler.getOb().get(n);
            if(g.getId()== ID.BLOCK){
                if(getBounds().intersects(g.getBounds())){
                    handler.removeObject(this);
                }
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillOval(x,y,8,8);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,8,8);
    }
}
